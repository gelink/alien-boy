var engineOfEngines = engineOfEngines || {};

engineOfEngines = {
  ctx: [],
  collisionEngine: {
    quadtree: null,
    colliding: function(thing1, thing2) {
      var thingOneX, thingTwoX, thingOneY, thingTwoY, thingOneHeight, thingOneWidth, thingTwoHeight, thingTwoWidth;

      thing1.collidybox.x = thing1.collidybox.x;
      thingTwoX = thing2.collidybox.x;
      thingOneY = thing1.collidybox.y;
      thingTwoY = thing2.collidybox.y;
      thingOneHeight = thing1.collidybox.height;
      thingTwoHeight = thing2.collidybox.height;
      thingOneWidth = thing1.collidybox.width;
      thingTwoWidth = thing2.collidybox.width;

      if (thing1.collidybox.x < thingTwo.x + thingTwo.width &&
         thing1.collidybox.x + thingOne.width > thingTwo.x &&
         thingOne.y < thingTwo.y + thingTwo.height &&
         thingOne.height + thingOne.y > thingTwo.y) {
          // collision detected!
        return true;
      }
      else {
        return false;
      }
    }
  },
  aiEngine: {
    moveAI: function() {
      /*var moveX, moveY;
      console.log(engineOfEngines.jakeokay);
      moveX = (engineOfEngines.alienBoy.positionX > engineOfEngines.jakeokay.positionX) ? engineOfEngines.jakeokay.SPEED : -1*engineOfEngines.jakeokay.SPEED;
      moveX = (engineOfEngines.alienBoy.positionX == engineOfEngines.jakeokay.positionX) ? 0 : moveX;
      engineOfEngines.jakeokay.positionX += moveX;

      moveY = (engineOfEngines.alienBoy.positionY > engineOfEngines.jakeokay.positionY) ? engineOfEngines.jakeokay.SPEED : -1*engineOfEngines.jakeokay.SPEED;
      moveY = (engineOfEngines.alienBoy.positionY == engineOfEngines.jakeokay.positionY) ? 0 : moveY;
      engineOfEngines.jakeokay.positionY += moveY;

      var ctx = engineOfEngines.ctx['enemy'];
      ctx.clearRect(0, 0, engineOfEngines.winW, engineOfEngines.winH);
      ctx.drawImage(
        $("#jakeokay")[0],
        0,
        0,
        156,
        176,
        engineOfEngines.jakeokay.positionX,
        engineOfEngines.jakeokay.positionY,
        156,
        176);*/
    }
  },
  sprengine: {
    currentAnimation: {
      name: 'wait',
      currentTime: 0
    },
    playAnimation: function() {
      var animation = engineOfEngines.alienBoy.frameAnimations[engineOfEngines.sprengine.currentAnimation.name];
      currentTime = engineOfEngines.sprengine.currentAnimation.currentTime;

      var frame;

      var frameIndex = parseInt(Math.floor((currentTime % animation.duration) / animation.refreshRate));
      frame = animation.frames[frameIndex];

      //animation.drawCanvas();
      //void ctx.drawImage(image, sourcex, sourcey, sWidth, sHeight, destinationx, destinationy, dWidth, dHeight)
      if(frame != null) {
        animation.currentFrame = frame;
        var ctx = engineOfEngines.ctx[animation.ctx];
        ctx.clearRect(0, 0, engineOfEngines.winW, engineOfEngines.winH);
        ctx.drawImage(
          $(animation.spriteID)[0],
          frame.x * animation.width,
          frame.y * animation.height,
          animation.width,
          animation.height,
          engineOfEngines.alienBoy.positionX,
          engineOfEngines.alienBoy.positionY,
          animation.width,
          animation.height);
      }

      engineOfEngines.sprengine.currentAnimation.currentTime += 60;
    }
  },

  keyengine: {
    keysDown: {},

    isDown: function(keyCode) {
      var keyCodes = keyCode instanceof Array ? keyCode : [keyCode];
      for (let key of keyCodes) {
        if (engineOfEngines.keyengine.keysDown[key]) return true;
      }

      return false;
    },

    buttonUp: function(e) {
      delete engineOfEngines.keyengine.keysDown[e.keyCode];
      if(!engineOfEngines.alienBoy.isMoving()) {
        //play waiting animation
        engineOfEngines.sprengine.currentAnimation.name = 'wait';
        engineOfEngines.sprengine.currentAnimation.currentTime = 0;
      }
    },

    buttonDown: function(e) {
      e.preventDefault();
      engineOfEngines.keyengine.keysDown[e.keyCode] = true;
      if(engineOfEngines.sprengine.currentAnimation.name == 'wait') {
        engineOfEngines.sprengine.currentAnimation.currentTime = 0;
      }
      engineOfEngines.sprengine.currentAnimation.name = 'walk';
    }
  },

  blink: function() {
    if($(".alien-boy .head").hasClass("blink")) {
      $(".alien-boy .head").removeClass("blink");
    }
    else if(!engineOfEngines.alienBoy.spitting) {
      $(".alien-boy .head").addClass("blink");
    }
  },

  generateBackground: function() {
    var ctx = document.getElementById("background").getContext("2d");
    var winH, winW;
    ctx.canvas.width  = winW = window.innerWidth;
    ctx.canvas.height = winH = window.innerHeight;
    engineOfEngines.ctx['bg'] = ctx;

    ctx = document.getElementById("alienboy").getContext("2d");
    ctx.canvas.width  = winW;
    ctx.canvas.height = winH;
    engineOfEngines.ctx['alienboy'] = ctx;

    ctx = document.getElementById("enemy").getContext("2d");
    ctx.canvas.width  = winW;
    ctx.canvas.height = winH;
    engineOfEngines.ctx['enemy'] = ctx;

    engineOfEngines.winW = winW;
    engineOfEngines.winH = winH;

    // draw pretty sunsetty background
    /*windowRadius = Math.max(window.innerWidth / 2, window.innerHeight / 2);
    vertCent = window.innerHeight / 2;
    horizCent =  window.innerWidth / 2;
    vertBelowCent = vertCent * 1.32;

    var radialgradient = ctx.createRadialGradient(horizCent, vertCent, windowRadius, horizCent, vertBelowCent, 40);


    radialgradient.addColorStop(0, '#f4adc4');
    radialgradient.addColorStop(0.18, '#ffb9b1');
    radialgradient.addColorStop(0.6, '#fff1c7');
    radialgradient.addColorStop(1, 'rgba(255,255,255,0)');

    ctx.fillStyle = radialgradient;
    ctx.fillRect(0, 0, window.innerWidth, window.innerHeight*2);

    var groundTiles = ['green', 'blue', 'purp', 'grey', 'multi', 'blugre'];

    var groundTileDetails = {
        'green':{x: 0, y: 512 }
      , 'blue':  {x: 196, y: 512 }
      , 'purp': {x: 392, y: 512 }
      , 'grey': {x: 588, y: 512 }
      ,'multi': {x: 784, y: 512 }
      , 'blugre': {x: 980, y: 512 }
    };

    // add tiles
    var tileH = 128;
    var tileW = 196;

    var groundH = Math.ceil(winH/tileH);
    var groundW = Math.ceil(winW/tileW);
    console.log(winH, winW);
    console.log(groundH, groundW);
    var background = [];

    for(var i=0; i < groundW; i++) {
      background[i] = [];

      for(var j=0; j < groundH; j++) {
        var tileOffset = Math.floor(Math.random() * 6);

        background[i][j] = tileOffset;

        ctx.drawImage($("#sprite")[0], groundTileDetails[groundTiles[tileOffset]].x, groundTileDetails[groundTiles[tileOffset]].y,  tileW, tileH,  i * tileW, j * tileH, tileW, tileH);
      }
    }*/
  },

  gameLoop: function() {
    //Moving character left and right
    if(engineOfEngines.keyengine.isDown([39, 68, 76])) {
      engineOfEngines.alienBoy.positionX += engineOfEngines.alienBoy.SPEED;
    }
    else if(engineOfEngines.keyengine.isDown([37, 65, 72])) {
      engineOfEngines.alienBoy.positionX -= engineOfEngines.alienBoy.SPEED;
    }

    //Moving character up and down
    if(engineOfEngines.keyengine.isDown([40])) {
      engineOfEngines.alienBoy.positionY += engineOfEngines.alienBoy.SPEED;
    }
    else if(engineOfEngines.keyengine.isDown([38])) {
      engineOfEngines.alienBoy.positionY -= engineOfEngines.alienBoy.SPEED;
    }

    if(engineOfEngines.keyengine.isDown([32]) && !engineOfEngines.alienBoy.spitting) {
      if(engineOfEngines.alienBoy.currentRounds > 0) {
        engineOfEngines.alienBoy.spitting = true;
        $(".ammunition").append("<div class='toothie toofshoof' style='left:"+(engineOfEngines.alienBoy.positionX+108)+"; top:"+(engineOfEngines.alienBoy.positionY+152)+";'></div>");
        $(".head").addClass("spit");

        //play pew pew
        var snd = new Audio("audio/pew1.wav"); // buffers automatically when created
        snd.play();

        engineOfEngines.alienBoy.currentRounds--;

        $("#ammo"+engineOfEngines.alienBoy.ammoMags).css("background-position-x", -1*((20-engineOfEngines.alienBoy.currentRounds)*98) +"px");

        if(engineOfEngines.alienBoy.currentRounds <= 0 && engineOfEngines.alienBoy.ammoMags > 0) {
          engineOfEngines.alienBoy.ammoMags--;
          engineOfEngines.alienBoy.currentRounds = 20;
        }
      }
      else {
        //play empty sound
        var snd = new Audio("audio/gun-click1.wav");
        snd.play();
      }
    }
    engineOfEngines.sprengine.playAnimation();
    engineOfEngines.aiEngine.moveAI();
  },
  jakeokay: {
    positionX: 740,
    positionY: 100,
    ctx: 'enemy',
    width: 156,
    height: 176,
    SPEED: 4
  },
  alienBoy: {
    render: function() {
      //shadow
      //draw pants
      //draw shirt
      //draw face
      //draw accesories

    },
    isMoving: function() {
      return engineOfEngines.keyengine.isDown([37,38,39,40]);
    },
    SPEED: 6,
    accesories: [],
    ctx: 'alienboy',
    positionX: 30,
    positionY: 200,
    width: 156,
    height: 256,
    health: 25,
    ammoMags: 1,
    currentRounds: 2,
    spitting: false,
    frameAnimations: {
      walk: {
        spriteID: '#sprite-wait',
        frames: [
                  {x:0, y:0}, {x:0, y:0}, {x:0, y:0},{x:0, y:0}, {x:0, y:0},
                  {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}
                ],
        duration: 600,
        refreshRate: 60,
        loop: true,
        width: 156,
        height: 176,
        currentTime: 0,
        currentFrame: 0,
        ctx: 'alienboy'
      },
      wait: {
        spriteID: '#sprite-wait',
        frames: [
                  {x:0, y:0}, {x:0, y:0}, {x:0, y:0},{x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0},
                  {x:0, y:1}, {x:0, y:1}, {x:0, y:1},{x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1},
                  {x:0, y:0}, {x:0, y:0}, {x:1, y:0},{x:2, y:0}, {x:2, y:0}, {x:1, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0},
                  {x:0, y:1}, {x:0, y:1}, {x:0, y:1},{x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1},
                  {x:0, y:0}, {x:0, y:0}, {x:0, y:0},{x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0},
                  {x:0, y:1}, {x:0, y:1}, {x:0, y:1},{x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1},
                  {x:0, y:0}, {x:0, y:0}, {x:0, y:0},{x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0},
                  {x:0, y:1}, {x:0, y:1}, {x:0, y:1},{x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1},
                  {x:0, y:0}, {x:0, y:0}, {x:0, y:0},{x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0},
                  {x:0, y:1}, {x:0, y:1}, {x:0, y:1},{x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1},
                  {x:0, y:0}, {x:0, y:0}, {x:0, y:0},{x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0},
                  {x:0, y:1}, {x:0, y:1}, {x:0, y:1},{x:0, y:1}, {x:1, y:1}, {x:2, y:1}, {x:2, y:1}, {x:1, y:1}, {x:0, y:1}, {x:0, y:1},
                  {x:0, y:0}, {x:0, y:0}, {x:0, y:0},{x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0},
                  {x:0, y:1}, {x:0, y:1}, {x:0, y:1},{x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1},
                  {x:0, y:0}, {x:0, y:0}, {x:0, y:0},{x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0},
                  {x:0, y:1}, {x:0, y:1}, {x:0, y:1},{x:0, y:1}, {x:1, y:1}, {x:2, y:1}, {x:2, y:1}, {x:1, y:1}, {x:0, y:1}, {x:0, y:1},
                  {x:8, y:0}, {x:10, y:0}, {x:10, y:0},{x:10, y:0}, {x:10, y:0}, {x:9, y:0}, {x:9, y:0}, {x:8, y:0}, {x:0, y:0}, {x:0, y:0},
                  {x:0, y:1}, {x:0, y:1}, {x:0, y:1},{x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1},
                  {x:0, y:0}, {x:0, y:0}, {x:0, y:0},{x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0},
                  {x:0, y:1}, {x:0, y:1}, {x:0, y:1},{x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1},
                  {x:0, y:0}, {x:0, y:0}, {x:0, y:0},{x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:0, y:0}, {x:1, y:0}, {x:2, y:0}, {x:2, y:0},
                  {x:1, y:1}, {x:0, y:1}, {x:0, y:1},{x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}, {x:0, y:1}
                ],
        duration: 13200,
        refreshRate: 60,
        loop: true,
        width: 156,
        height: 176,
        currentTime: 0,
        currentFrame: 0,
        ctx: 'alienboy'
      },
      spit: {
        spriteID: '#sprite',
        frames: 3,
        frameOffsets: [{x:196, y:768}, {x:392, y:768}, {x:588, y:768}],
        keyFrames: {},
        duration: 600,
        loop: false
      }
    }
  }
};

var SPEED = 6;

$(window).on('keydown', function(event) {
  engineOfEngines.keyengine.buttonDown(event);
});

$(window).on('keyup', function(event) {
  engineOfEngines.keyengine.buttonUp(event);
});

$(window).on('animationend webkitAnimationEnd oanimationend MSAnimationEnd', function(event) {
  $(event.target).removeClass(event.originalEvent.animationName);

  if(event.originalEvent.animationName == 'spit') {
    engineOfEngines.alienBoy.spitting = false;
  }
});

//window.setInterval(function(){engineOfEngines.sprengine.playAnimation('bop');}, 1200);

window.setInterval(engineOfEngines.gameLoop, 60);


engineOfEngines.generateBackground();
engineOfEngines.sprengine.playAnimation('wait');

//window.addEventListener("resize", resizeCanvas, false);

function resizeCanvas() {
  // resize all canvas..
    $("canvas").style.width = window.innerWidth + 'px';
    $("canvas").style.height = window.innerHeight + 'px';
}